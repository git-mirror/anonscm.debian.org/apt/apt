#!/bin/sh
set -e

TESTDIR="$(readlink -f "$(dirname "$0")")"
. "$TESTDIR/framework"

setupenvironment
configarchitecture "i386"

DESCR='Some description that has a unusual word xxyyzz and aabbcc and a UPPERCASE'
DESCR2='Some other description with the unusual aabbcc only'
insertpackage 'unstable' 'foo' 'all' '1.0' '' '' "$DESCR
 Long description of stuff and such, with lines
 .
 and paragraphs and everything."
insertpackage 'testing' 'bar' 'i386' '2.0' '' '' "$DESCR2"

setupaptarchive

APTARCHIVE="$(readlink -f ./aptarchive)"

cat >> json-hook.sh << EOF
#!/bin/bash
while true; do
	read request <&\$APT_HOOK_SOCKET
	read empty <&\$APT_HOOK_SOCKET

	if echo "\$request" | grep -q ".hello"; then
		echo "HOOK: HELLO"
		printf '{"jsonrpc": "2.0", "result": {"version": "0.1"}, "id": 0}\n\n' >&\$APT_HOOK_SOCKET
	fi

	if echo "\$request" | grep -q ".bye"; then
		echo "HOOK: BYE"
		exit 0;
	fi

	echo HOOK: request \$request
	echo HOOK: empty \$empty
done
EOF

chmod +x json-hook.sh

HOOK="$(readlink -f ./json-hook.sh)"

# Setup all hooks
cat >> rootdir/etc/apt/apt.conf.d/99-json-hooks << EOF
	AptCli::Hooks::Install:: "$HOOK";
	AptCli::Hooks::Search:: "$HOOK";
EOF


############################# Success search #######################
testsuccessequal 'HOOK: HELLO
HOOK: request {"jsonrpc":"2.0","method":"org.debian.apt.hooks.hello","id":0,"params":{"versions":["0.1"]}}
HOOK: empty
HOOK: request {"jsonrpc":"2.0","method":"org.debian.apt.hooks.search.pre","params":{"command":"search","search-terms":["foo"],"unknown-packages":[],"packages":[]}}
HOOK: empty
HOOK: BYE
Sorting...
Full Text Search...
foo/unstable 1.0 all
  Some description that has a unusual word xxyyzz and aabbcc and a UPPERCASE

HOOK: HELLO
HOOK: request {"jsonrpc":"2.0","method":"org.debian.apt.hooks.hello","id":0,"params":{"versions":["0.1"]}}
HOOK: empty
HOOK: request {"jsonrpc":"2.0","method":"org.debian.apt.hooks.search.post","params":{"command":"search","search-terms":["foo"],"unknown-packages":[],"packages":[]}}
HOOK: empty
HOOK: BYE' apt search foo

############################# Failed search #######################
testsuccessequal 'HOOK: HELLO
HOOK: request {"jsonrpc":"2.0","method":"org.debian.apt.hooks.hello","id":0,"params":{"versions":["0.1"]}}
HOOK: empty
HOOK: request {"jsonrpc":"2.0","method":"org.debian.apt.hooks.search.pre","params":{"command":"search","search-terms":["foox"],"unknown-packages":[],"packages":[]}}
HOOK: empty
HOOK: BYE
Sorting...
Full Text Search...
HOOK: HELLO
HOOK: request {"jsonrpc":"2.0","method":"org.debian.apt.hooks.hello","id":0,"params":{"versions":["0.1"]}}
HOOK: empty
HOOK: request {"jsonrpc":"2.0","method":"org.debian.apt.hooks.search.fail","params":{"command":"search","search-terms":["foox"],"unknown-packages":[],"packages":[]}}
HOOK: empty
HOOK: BYE' apt search foox


############################# Failed install #######################

testfailureequal 'Reading package lists...
Building dependency tree...
HOOK: HELLO
HOOK: request {"jsonrpc":"2.0","method":"org.debian.apt.hooks.hello","id":0,"params":{"versions":["0.1"]}}
HOOK: empty
HOOK: request {"jsonrpc":"2.0","method":"org.debian.apt.hooks.install.fail","params":{"command":"install","search-terms":["foxxx"],"unknown-packages":["foxxx"],"packages":[]}}
HOOK: empty
HOOK: BYE
E: Unable to locate package foxxx' apt install foxxx

############################# Success install #######################

testsuccessequal 'Reading package lists...
Building dependency tree...
HOOK: HELLO
HOOK: request {"jsonrpc":"2.0","method":"org.debian.apt.hooks.hello","id":0,"params":{"versions":["0.1"]}}
HOOK: empty
HOOK: request {"jsonrpc":"2.0","method":"org.debian.apt.hooks.install.pre-prompt","params":{"command":"install","search-terms":["foo"],"unknown-packages":[],"packages":[{"id":1,"name":"foo","architecture":"i386","mode":"install","automatic":false,"versions":{"candidate":{"id":1,"version":"1.0","architecture":"all","pin":500},"install":{"id":1,"version":"1.0","architecture":"all","pin":500}}}]}}
HOOK: empty
HOOK: BYE
The following NEW packages will be installed:
  foo
0 upgraded, 1 newly installed, 0 to remove and 0 not upgraded.
Inst foo (1.0 unstable [all])
Conf foo (1.0 unstable [all])
HOOK: HELLO
HOOK: request {"jsonrpc":"2.0","method":"org.debian.apt.hooks.hello","id":0,"params":{"versions":["0.1"]}}
HOOK: empty
HOOK: request {"jsonrpc":"2.0","method":"org.debian.apt.hooks.install.post","params":{"command":"install","search-terms":["foo"],"unknown-packages":[],"packages":[{"id":1,"name":"foo","architecture":"i386","mode":"install","automatic":false,"versions":{"candidate":{"id":1,"version":"1.0","architecture":"all","pin":500},"install":{"id":1,"version":"1.0","architecture":"all","pin":500}}}]}}
HOOK: empty
HOOK: BYE' apt install foo -s
